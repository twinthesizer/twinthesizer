import md5 from 'md5'
import credentials from '../keys.js'

import Twit from 'twit'
import Translator from 'watson-developer-cloud/language-translator/v3'
import LanguageUnderstanding from 'watson-developer-cloud/natural-language-understanding/v1'

const supportedLanguages = ['af', 'ar', 'az', 'ba', 'be', 'bg', 'bn', 'bs', 'cs', 'cv', 'da', 'de', 'el', 'en', 'eo', 'es', 'et', 'eu', 'fa', 'fi', 'fr', 'gu', 'he', 'hi', 'ht', 'hu', 'hy', 'id', 'is', 'it', 'ja', 'ka', 'kk', 'km', 'ko', 'ku', 'ky', 'lt', 'lv', 'ml', 'mn', 'nb', 'nl', 'nn', 'pa', 'pl', 'ps', 'pt', 'ro', 'ru', 'sk', 'so', 'sq', 'sv', 'ta', 'te', 'tr', 'uk', 'ur', 'vi', 'zh']

const T = new Twit({
    ...credentials.twitter,
    timeout_ms: 60 * 1000,
    strictSSL: true
})

const translator = new Translator({
    ...credentials.ibm.translator,
    version: '2018-05-01'
})

const languageUnderstanding = new LanguageUnderstanding({
    ...credentials.ibm.languageUnderstanding,
    version: '2018-03-16'
})

function _analyze(data, callback) {
    try {
        languageUnderstanding.analyze({
            html: data,
            features: {
                emotion: {},
                sentiment: {}
            }
        }, (err, response) => {
            if (err) console.error(err)

            callback({ sentiment: response.sentiment.document, emotion: response.emotion.document.emotion })
        })
    } catch (err) {

    }
}

class TwitterStream {

    constructor(keyword = 'jugendhackt', callback) {
        console.log('as')
        this.stream = T.stream('statuses/filter', { track: keyword })
        this.stream.on('tweet', function (tweet) {
            console.log(tweet)
            const { id_str, text, user, lang, timestamp_ms } = tweet
            const { name, screen_name, profile_image_url_https } = user
            const hash = md5(text)
            const plaintext = text.replace(/(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])/gi, '...').replace(/[^a-z0-9\s.,!?:]/gi, '')

            if (!supportedLanguages.includes(lang)) {
                return
            }

            if (lang.toLowerCase() === 'en') {
                _analyze(plaintext, data => {
                    callback({ ...data, id: id_str, timestamp: new Date(Number.parseInt(timestamp_ms)) })
                })
            } else {
                translator.translate({ text: plaintext, source: lang, target: 'en' }, (err, translation) => {
                    if (err) console.error(err)

                    try {
                        translation = translation.translations[0].translation

                        _analyze(translation, data => {
                            callback({ ...data, id: id_str, timestamp: new Date(Number.parseInt(timestamp_ms)) })
                        })
                    } catch (err) {

                    }

                })
            }
        })
    }
}

export default TwitterStream

// const stream = T.stream('statuses/filter', {track: 'jugendhackt'})
// stream.on('tweet', tweet => {

// })

// class TwitterStream {

//     constructor(keyword = 'jugendhackt', callback = t => {console.log(t)}) {
//         this.stream = T.stream('statuses/filter', {track: keyword})
//         this.stream.on('tweet', tweet => {
//             const { text, user, lang, timestamp_ms } = tweet
//             const { name, screen_name, profile_image_url_https } = user
//             const hash = md5(text)
//             const plaintext = text.replace(/(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])/gi, '...').replace(/[^a-z0-9\s.,!?:]/gi, '')


//         })
//     }

// }

// stream.on('tweet', function (tweet) {
//     const { id, text, user, lang, timestamp_ms } = tweet
//     const { name, screen_name, profile_image_url_https } = user
//     const hash = md5(text)
//     const plaintext = text.replace(/(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])/gi, '...').replace(/[^a-z0-9\s.,!?:]/gi, '')

//     console.log({id, text, plaintext, lang, timestamp_ms: new Date(Number.parseInt(timestamp_ms)), name, screen_name, profile_image_url_https, hash})
// })