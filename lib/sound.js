const patterns = {
    joy: ['D4', 'F#4', 'A4', 'D5', 'D4', 'F#4', 'A4', 'F#4', 'D4', 'F#4', 'A4', 'D5', 'f#4', 'A4', 'D5', 'F#5'],
    fear: ['B3', 'A6', 'D#3', 'A4', 'B1', 'B3', 'B1', 'D#3', 'A6', 'B3', 'D#3', 'B3', 'A6', 'B3', 'D#3', 'F#8'],
    anger: ['C3','B6','D#3', 'E#4', 'B1', 'B3', 'C1', 'D#3', 'G#6', 'B3', 'D#3', 'C#3', 'A6', 'B3', 'D#3', 'F#8'],
    sadness: ['C4', 'D4', 'Eb4', 'F4', 'G4', 'Ab4', 'G3', 'Bb4', 'C4', 'D4', 'Eb4', 'F4', 'G4', 'Ab4', 'G3', 'B', 'Bb4'],
    disgust: ['E7', 'B6', 'C#', 'G0', 'D6', 'A0', 'E6','C6', 'E7', 'B6', 'C#', 'G0', 'D6', 'A0', 'E6','C6']
}


/*

anger: 0.100001 #
disgust: 0.074066
fear: 0.100392 #
joy: 0.206367 #
sadness: 0.137498 #

*/

function dataToNote(data) {
    const { emotion, sentiment } = data

    console.log(data)

    const keys = Object.keys(emotion)
    keys.sort((a, b) => {
        return emotion[b] - emotion[a]
    })

    const strongestEmotion = keys.slice(0, 1).join('')

    return patterns[strongestEmotion] || ['D4','D4','D4','D4','D4','D4','D4','D4','D4','D4','D4','D4']

}

export default {
    dataToNote
}