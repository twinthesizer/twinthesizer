import http from 'http'

import Socket from 'socket.io'
import Koa from 'koa'

import logger from 'koa-logger'
import views from 'koa-views'
import serve from 'koa-static'

import TwitterStream from './lib/twitterStream'
import sound from './lib/sound'

const app = new Koa()

app.use(logger())

app.use(serve(__dirname + '/page/static'))
app.use(views(__dirname + '/page/views'))

app.use(async (ctx, next) => {
    await ctx.render('index')
})

const httpServer = http.createServer(app.callback())
const io = new Socket(httpServer)

io.on('connection', socket => {

    socket.on('startStream', hashtag => {
        console.log(hashtag)
        // data: sentiment, emotion, id, timestamp
        const stream = new TwitterStream(hashtag, data => {
            
            data.tones = sound.dataToNote(data)

            console.log(data)
            io.emit('playSound', data)

        })
    })

})

httpServer.listen(3000, _ => {
    console.log('Listening on port 3000')
})