# twinthesizer

## Installation
1. Das Repository mit `$ git clone https://gitlab.com/twinthesizer/twinthesizer.git` clonen

2. Einen IBM Watson Bluemix Account anlegen und Credentials für die `Natural Language Understanding` und die `Language Translator`  anlegen

3. Twitter Account als Developer-Account registrieren 

4. Die IBM & Twitter Keys in folgender Struktur in der `keys.js` Datei im Projekt-Root speichern:

```js
module.exports = {
   twitter: {
       consumer_key: <consumer_key>,
       consumer_secret: <consumer_secret>,
   
       access_token: <access_token>,
       access_token_secret: <access_token_secret>
   },
   ibm: {
       translator: {
           url: 'https://gateway.watsonplatform.net/language-translator/api',
           username: <username>,
           password: <password>
       },
       languageUnderstanding: {
           url: 'https://gateway.watsonplatform.net/natural-language-understanding/api',
           username: <username>,
           password: <password>
       }
   }
}
```

5. Abhängigkeiten mit `$ npm install` bzw. `$ yarn` installieren

6. Den Server mit `$ node index.js` starten

7. Auf http://localhost:3000 navigieren, einen Hashtag eintragen und

8. wunderschöne Musik genießen!